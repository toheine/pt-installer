#!/bin/bash

# PT8-Installer is a helper-script for installing Packet-Tracer on a Non-Deb-System
#
# Idea: Andreas Grupp -> https://grupp-web.de
#
# March 2021
# 
# Tobias Heine <toheine@posteo.de>
# GPLv3

## Settings

wrkspc=/tmp/PacketTracerInst
file=$1

## Functions

unpack() {
    rm -rf $wrkspc > /dev/null 2>&1
    mkdir $wrkspc
    cp "$file" "$wrkspc"
    cd "$wrkspc" || exit
    ar -xv "$file"
    mkdir control
    tar -C control -Jxf control.tar.xz
    mkdir data
    tar -C data -Jxf data.tar.xz
}

removeCurInst() {
    rm -rf /opt/pt
    rm -rf /usr/share/icons/hicolor/48x48/apps/pt7.png
    xdg-desktop-menu uninstall /usr/share/applications/cisco-pt.desktop
    xdg-desktop-menu uninstall /usr/share/applications/cisco-ptsa.desktop
    xdg-desktop-menu uninstall /usr/share/applications/cisco-pt7.desktop
    xdg-desktop-menu uninstall /usr/share/applications/cisco-ptsa7.desktop
    rm -rf /usr/share/applications/cisco-pt.desktop
    rm -rf /usr/share/applications/cisco-ptsa.desktop
    rm -rf /usr/share/applications/cisco-pt7.desktop
    rm -rf /usr/share/applications/cisco-ptsa7.desktop
    update-mime-database /usr/share/mime
    gtk-update-icon-cache --force /usr/share/icons/gnome
    rm -f /usr/local/bin/packettracer
}

install() {
    cd "$wrkspc"/data || exit
    cp -r usr /
    cp -r opt /
    if test ! -L /usr/lib64/libdouble-conversion.so.1; then
    	ln -s /usr/lib64/libdouble-conversion.so.3.1.5 /usr/lib64/libdouble-conversion.so.1
    fi
    # update icon and file assocation
    echo -e "[Desktop Entry]\nType=Application\nExec=/opt/pt/packettracer %f\nName=Packet Tracer 8.2.2\nIcon=/opt/pt/art/app.png\nTerminal=false\nStartupNotify=true\nMimeType=application/x-pkt;application/x-pka;application/x-pkz;application/x-pks;application/x-pksz;" > /usr/share/applications/cisco-pt.desktop
    echo -e "[Desktop Entry]\nType=Application\nExec=/opt/pt/packettracer -uri=%u\nName=Packet Tracer 8.2.2\nIcon=/opt/pt/art/app.png\nTerminal=false\nStartupNotify=true\nNoDisplay=true\nMimeType=x-scheme-handler/pttp;" > /usr/share/applications/cisco-ptsa.desktop
    xdg-desktop-menu install /usr/share/applications/cisco-pt.desktop
    xdg-desktop-menu install /usr/share/applications/cisco-ptsa.desktop
    update-mime-database /usr/share/mime
    gtk-update-icon-cache --force --ignore-theme-index /usr/share/icons/gnome
    xdg-mime default cisco-ptsa.desktop x-scheme-handler/pttp
    ln -sf /opt/pt/packettracer /usr/local/bin/packettracer
}

updateEnvironment(){
    if grep -q "PT8HOME" /etc/profile; then
        echo "export PT8HOME=/opt/pt" >> /etc/profile
    fi
    if grep -q "QT_DEVICE_PIXEL_RATIO" /etc/profile; then
        echo "export QT_DEVICE_PIXEL_RATIO=auto" >> /etc/profile
    fi
}

updateStartupScripts(){
    # On systems like openSUSE Tumbleweed the Qt WebEngine is sandboxed by default. This leads
    # in PacketTracer to the effect that the login page is just an empty white window, and later
    # instructions are also not rendered and show also only an empty white window. So this
    # changes sandboxing for PacketTracer - it is turned off
    # See also https://doc.qt.io/qt-5/qtwebengine-platform-notes.html#sandboxing-support
    echo "   Turn off Qt WebEngine sandbox in /usr/local/bin/packettracer"
    sed -i '/^export LD_LIBRARY_PATH.*/a export QTWEBENGINE_DISABLE_SANDBOX=1' /usr/local/bin/packettracer
    echo "   Turn off Qt WebEngine sandbox in /opt/pt/packettracer"
    sed -i '/^export LD_LIBRARY_PATH.*/a export QTWEBENGINE_DISABLE_SANDBOX=1' /opt/pt/packettracer
}

usage(){
    echo ""
    echo "Usage example:"
    echo "sudo $0 CiscoPacketTracer_810_Ubuntu_64bit.deb"
    echo ""
}

## Script Start

# Check if the script is executed as root
if [ "$(whoami)" != root ]; then
    echo "Run this script as root."
    usage
    exit 1
fi

# Check if the script-Call includes one parameter
if [ $# -eq 0 ]; then
    echo "Run this script with a Packettracer8-Installation-File from https://netacad.com."
    usage
    exit 2
fi

# Check if given Installation-File seems to be an Packet-Tracer 8
if [[ "$1" != *Packet*Tracer8*.deb ]];then
    echo "The installation file is not the right version for this installer!"
    usage
    exit 3
fi

echo "-> Unpack PT-Files in $wrkspc"
unpack

echo "-> Remove Current Installation"
removeCurInst

echo "-> Install Packet Tracer 8"
install

echo "-> Update Environment Variables"
updateEnvironment

echo "-> Update startup scripts"
updateStartupScripts

echo "Done!"
